import os
import numpy as np
from flask import Flask, request, render_template, redirect, jsonify
import joblib
from PIL import Image
import base64
import io

app = Flask(__name__)

# Load the trained logistic regression model
model_logistic_regression = joblib.load('gender_classification_model.pkl')

# Define a function to preprocess images
def preprocess_image(image_data, image_size=(150, 150)):
    try:
        image = Image.open(image_data)
        image = image.resize(image_size)
        image = image.convert('RGB')
        image_array = np.array(image) / 255.0
        return image_array.flatten()
    except Exception as e:
        print(f"Error processing image: {e}")
        return None

# Define the categories
Categories = ['Male', 'Female']

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/scan')
def scan():
    return render_template('index.html')

@app.route('/classify', methods=['GET', 'POST'])
def classify():
    if request.method == 'POST':
        if 'file' not in request.files:
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            return redirect(request.url)
        if file:
            if not os.path.exists('uploads'):
                os.makedirs('uploads')
            
            file_path = os.path.join('uploads', file.filename)
            file.save(file_path)
            
            image_array = preprocess_image(file_path)
            if image_array is not None:
                image_array = image_array.reshape(1, -1)
                prediction_logistic_regression = model_logistic_regression.predict(image_array)
                predicted_class_logistic_regression = Categories[prediction_logistic_regression[0]]
                
                image = Image.open(file_path)
                result_image_path = os.path.join('static', 'result.png')
                image.save(result_image_path)
                
                return render_template('result.html', predicted_class_logistic_regression=predicted_class_logistic_regression, image_url=result_image_path)
    
    return render_template('index.html')

@app.route('/classify_camera', methods=['POST'])
def classify_camera():
    image_data = request.form['image_data']
    image_data = base64.b64decode(image_data.split(',')[1])
    image = Image.open(io.BytesIO(image_data))

    if not os.path.exists('uploads'):
        os.makedirs('uploads')

    image_path = os.path.join('uploads', 'captured_image.png')
    image.save(image_path)

    image_array = preprocess_image(image_path)
    if image_array is not None:
        image_array = image_array.reshape(1, -1)
        prediction_logistic_regression = model_logistic_regression.predict(image_array)
        predicted_class_logistic_regression = Categories[prediction_logistic_regression[0]]
        
        result_image_path = os.path.join('static', 'result.png')
        image.save(result_image_path)

        return jsonify({
            'predicted_class_logistic_regression': predicted_class_logistic_regression,
            'image_url': result_image_path
        })
    else:
        return jsonify({'error': 'Image processing failed'}), 400

@app.route('/result', methods=['GET'])
def result():
    predicted_class_logistic_regression = request.args.get('predicted_class_logistic_regression')
    image_url = request.args.get('image_url')
    return render_template('result.html', predicted_class_logistic_regression=predicted_class_logistic_regression, image_url=image_url)

if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port, debug=True)

